#!/bin/bash

# inicializacao do container
if [ "$1" == "start" ]; then

  # no caso de nao encontrar wallet, cria um novo
  if [ ! -f /coin/data/wallet ]; then
    echo "1 exit" | /coin/bin/walletd-cli --generate-new-wallet /coin/data/wallet --password password --log-file /coin/data/walletd-cli.log > /coin/data/wallet.log
  fi

  # inicia o processo coind e walletd
  /usr/local/bin/supervisord --nodaemon --configuration /etc/supervisord.conf

# qualquer outro comando
else
  exec "$@"
fi
