CREATE TABLE IF NOT EXISTS `order`
(
  id CHAR(73) NOT NULL PRIMARY KEY,
  type CHAR(4) NULL,
  coinSymbol VARCHAR(45) NULL,
  coinAmount DOUBLE NULL,
  coinFee DOUBLE NULL,
  unitPrice DOUBLE NULL,
  baseCoinSymbol VARCHAR(45) NULL,
  baseCoinAmount DOUBLE NULL,
  baseCoinFee DOUBLE NULL,
  buyerUserId CHAR(36) NULL,
  sellerUserId CHAR(36) NULL,
  date datetime NULL
);
