FROM ubuntu:16.04

COPY docker-entrypoint.sh /usr/bin/docker-entrypoint.sh
COPY supervisord.conf /etc/supervisord.conf
COPY coind.conf /coin/coind.conf
COPY walletd.conf /coin/walletd.conf

RUN apt-get update \
 && apt-get install -y git build-essential cmake pkg-config \
                       libboost-all-dev libssl-dev libzmq3-dev \
                       libunbound-dev libsodium-dev python-pip \
 && pip install supervisor \
 && pip install git+https://github.com/bendikro/supervisord-dependent-startup.git@v1.1.0 \
 && mkdir -p /coin/data \
 && mkdir -p /coin/bin \
 && mkdir -p /var/log/supervisord \
 && cd /coin \
 && git clone --recursive https://github.com/BloodDonationCoin/BloodDonationCoin.git daemon \
 && cd /coin/daemon \
 && make \
 && cp --preserve /coin/daemon/build/release/bin/* /coin/bin/.  \
 && ln -s /coin/bin/blooddonationcoind /coin/bin/coind \
 && ln -s /coin/bin/blooddonationcoin-wallet-rpc /coin/bin/walletd \
 && ln -s /coin/bin/blooddonationcoin-wallet-cli /coin/bin/walletd-cli \
 && cd /coin \
 && rm -fR /coin/daemon \
 && chmod +x /usr/bin/docker-entrypoint.sh \
 && ln -s /usr/bin/docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR /coin

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
