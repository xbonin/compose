#! /bin/sh

if [ ! -f /coin/data/wallet ]; then
    /coin/daemon/walletd -w /coin/data/wallet -p password -g
fi

/coin/daemon/walletd --data-dir /coin/data -w /coin/data/wallet -p password --local --bind-address 0.0.0.0  --bind-port 18332
