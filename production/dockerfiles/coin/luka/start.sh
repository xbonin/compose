#! /bin/sh

if [ ! -f /coin/data/wallet ]; then
    /coin/daemon/cryptonote/build/src/walletd -w /coin/data/wallet -p password -g
fi

/coin/daemon/cryptonote/build/src/walletd --data-dir /coin/data -w /coin/data/wallet -p password --local --bind-address 0.0.0.0 --bind-port 18332
