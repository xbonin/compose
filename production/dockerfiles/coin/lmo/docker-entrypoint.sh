#!/bin/bash

# inicializacao do container
if [ "$1" == "start" ]; then

  # no caso de nao encontrar wallet, cria um novo
  if [ ! -f /coin/data/wallet ]; then
    /coin/bin/walletd-cli -w /coin/data/wallet -p password -g > /coin/data/log/newwallet.log
  fi

  # inicia o processo coind e walletd
  /usr/local/bin/supervisord --nodaemon --configuration /etc/supervisord.conf

# qualquer outro comando
else
  exec "$@"
fi
